import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuard } from './services/authentication/auth.guard';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'pages/login',
        pathMatch: 'full',
    }, {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                //canActivate: [AuthGuard] ,
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }, 
            {
                path: 'administration',
                //canActivate: [AuthGuard] ,
                loadChildren: './administration/administration.module#AdministrationModule'
            },
            {
                path: 'project',
                //canActivate: [AuthGuard] ,
                loadChildren: './projects/projects.module#ProjectModule'
            },
            {
                path: 'vehicle',
                //canActivate: [AuthGuard] ,
                loadChildren: './vehicles/vehicle.module#VehicleModule'
            }
        ]
    }, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        }]
    }
];

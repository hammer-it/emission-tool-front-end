export const MOTORWERKZAAMHEDENTIP = "Als het voertuig ook wordt ingezet voor het uitvoeren van werkzaamheden anders dan het rijden, vul dan hier de gegevens van de motor voor het uitvoeren van werkzaamheden. Let op, deze gegevens kunnen anders zijn dan de motorgegevens voor het rijden.";
export const AANDRIJVINGTIP = "Heeft de motor een constant of variabel toerental?";
export const VERMOGENTIP = "<b>Let op!</b><br> Wanneer de motor voor het rijden ook wordt gebruikt voor werkzaamheden kan het voorkomen dat het toerental en het vermogen wordt begrensd en teruggebracht. <br><br> Bijvoorbeeld:<br> een motor van 400 kW voor het rijden van een zwaar voertuig wordt voor werkzaamheden terugbracht naar ongeveer 175 kW. Vul dan hier 175 kW in."
export const FILTERTIP = "Wordt er naast een ingebouwd filtersysteem nog een additioneel filter aan de motor bevestigd?";
export const GEWICHTTIP = "Vul hier het basisgewicht van het voertuig in. Pas dit eventueel aan bij een project. Neem dan het gemiddelde gewicht van het voertuig en de lading over de gehele afstand (bijvoorbeeld 60 ton heen en 40 ton terug is 50 ton gemiddeld)."
export const AFSTANDTIP = "vul de totaal afgelegde afstand in zowel heen als terug. Als u een voertuig meerdere dagen laat rijden telt u deze afstand bij elkaar op."

export const VERSIEKENMERKTIP = `Vul hier een versiekenmerk in. Bij het opslaan van een concept kan dit leeggelaten worden.
<br><br>
<b>Concept</b><br>
 Wanneer je een versie als concept opslaat, zat dit het eerste project zijn die wordt ingeladen. Het concept wordt gebruikt wanneer je veranderingen wil opslaan maar wanneer deze nog niet definitief zijn voor een versie.
<br><br> 
<b>Nieuwe versie</b><br>
Wanneer je een nieuwe versie opslaat zul je deze in de toekomst kunnen vergelijken met een andere versie in een nieuw overzicht.`
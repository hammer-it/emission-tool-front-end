export const TypeProductsubscriptions = [
    { 
        type: 'Licentie met 1 gebruiker',
        aantalGebruikers: 1,
        prijs: '€22 / maand', 
    },
    { 
        type: 'Licentie t/m 5 gebruikers',
        aantalGebruikers: 5,
        prijs: '€30 / maand', 
    },
    { 
        type: 'Licentie t/m 10 gebruikers',
        aantalGebruikers: 10,
        prijs: '€35 / maand', 
    },
    { 
        type: 'Licentie onbeperkt',
        aantalGebruikers: 25,
        prijs: '€40 / maand', 
    }
]
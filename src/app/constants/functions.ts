//import { VehicleEmission } from "app/classes/vehicleEmission";

export function deepEquals(x, y) {
    if (x === y) {
      return true; // if both x and y are null or undefined and exactly the same
    } else if (!(x instanceof Object) || !(y instanceof Object)) {
      return false; // if they are not strictly equal, they both need to be Objects
    } else if (x.constructor !== y.constructor) {
      // they must have the exact same prototype chain, the closest we can do is
      // test their constructor.
      return false;
    } else {
      for (const p in x) {
        if (!x.hasOwnProperty(p)) {
          continue; // other properties were tested using x.constructor === y.constructor
        }
        if (!y.hasOwnProperty(p)) {
          return false; // allows to compare x[ p ] and y[ p ] when set to undefined
        }
        if (x[p] === y[p]) {
          continue; // if they have the same strict value or identity then they are equal
        }
        if (typeof (x[p]) !== 'object') {
          return false; // Numbers, Strings, Functions, Booleans must be strictly equal
        }
        if (!deepEquals(x[p], y[p])) {
          return false;
        }
      }
      for (const p in y) {
        if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) {
          return false;
        }
      }
      return true;
    }
  }

  
export async function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export function deepCopy(obj) {
  
  var copy;

  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) return obj;

  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }

  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (var i = 0, len = obj.length; i < len; i++) {
      copy[i] = deepCopy(obj[i]);
    }
    return copy;
  }

  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = deepCopy(obj[attr]);
    }
    return copy;
  }

  throw new Error("Unable to copy obj! Its type isn't supported.");
}

export function customOrderBy(values: any[], orderType: any) { 
  return values.sort((a, b) => {
      if (a[orderType] < b[orderType]) {
          return -1;
      }

      if (a[orderType] > b[orderType]) {
          return 1;
      }

      return 0
  });
}

export function getSafe(fn, defaultVal) {
  try {
      return fn();
  } catch (e) {
      return defaultVal;
  }
}

export function getDisclaimer(){
  var disclaimer = "De getoonde waarden geven een inschatting van de maximale uitstoot bij het project. "+
  "Deze waarden kunnen afwijken met de werkelijke uitstoot. De VVT aanvaardt geen aansprakelijkheid voor de getoonde waarden. "+ 
  "Aan de met behulp van door de gebruiker ingevoerde gegevens tot stand gekomen berekeningen kunnen geen rechten worden ontleend. "+ 
  "Voor vragen over het rekenmodel kunt u contact opnemen met de VVT via info@verticaaltransport.nl."

  return disclaimer
}

declare const $: any;
export function showNotification(align: any, msg: string, color: string ) {
    const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

    $.notify({
        icon: 'notifications',
        message: msg
    }, {
        type: color,
        timer: 3000,
        placement: {
            from: 'top',
            align: align
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-with-icon" role="alert">' +
              '<button mat-raised-button type="button" aria-hidden="true" class="close" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
              '<i class="material-icons" data-notify="icon">notifications</i> ' +
              '<span data-notify="title">{1}</span> ' +
              '<span data-notify="message">{2}</span>' +
              '<div class="progress" data-notify="progressbar">' +
                  '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
              '</div>' +
              '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>'
    });
}



export function checkFields(vehicle) {
  var canContinue = false;

  if(vehicle.vehicle.activity.dtol > 100){
    vehicle.vehicle.activity.dtol = 100
  }

  if (vehicle.vehicle.brand == "" || vehicle.vehicle.model == "" || vehicle.vehicle.modelYear == 0 || vehicle.vehicle.vehicleType == "" || vehicle.vehicle.weight < 0 || vehicle.vehicle.activity.description.length < 1 || vehicle.vehicle.activity.distance < 0 || vehicle.vehicle.activity.dtol < 0 || vehicle.vehicle.activity.tol < 0 || vehicle.vehicle.activity.workload < 0) {
    showNotification('center',"Niet alle velden zijn ingevuld of er zijn verkeerde waarden ingevuld.", 'danger')
    canContinue = false;
    return canContinue;
  }

  for (const [key, value] of Object.entries(vehicle.vehicle.primaryEngine)) {
    //  //console.log(value)
    if (value === "" || value === 0) {
      //console.log(value)
      showNotification('center',"Primaire motor niet correct ingevuld.", 'danger')
      canContinue = false
      return canContinue;
    }
  }
 
    for (const [key, value] of Object.entries(vehicle.vehicle.secundairyEngine)) {
      if (value === "" || value === 0 || value == null) {
        //console.log(value)
        showNotification("center","Motor werkzaamheden niet correct ingevuld.", 'danger')
        canContinue = false
        return canContinue;
      }
    }


  //als hij draai werkzaamheden uitvoert:
  if(vehicle.vehicle.activity.dtol || vehicle.vehicle.activity.workload){
    if(!vehicle.vehicle.activity.dtol || !vehicle.vehicle.activity.workload || !vehicle.vehicle.activity.tol || vehicle.vehicle.activity.workload <= 0){
      showNotification('center',"Er is geen draaitijd, werkbelasting of tijd op locatie ingevuld. Werkbelasting kan niet nul zijn bij werkzaamheden.", 'danger')
      canContinue = false
      return canContinue;
    }
  }

  //Geen werkzaamheden en geen rijafstand
  if(vehicle.vehicle.activity.workload <= 0 && vehicle.vehicle.activity.distance <= 0){
    showNotification('center',"Er zijn geen werkzaamheden en geen rijafstand ingevuld. Vul minimaal een van de twee in.", 'danger')
    canContinue = false
    return canContinue;
  }

  canContinue = true

  return canContinue
}
// Graag bij typevoertuig nu voorlopig de typen: dumper, wals, shovel, laadschop en kiepbak niet to-nen. Deze komen kunnen in latere versie getoond worden.
export const typeVoertuig = [
  { value: 'Mobiele telescoopkraan', viewValue: 'Mobiele telescoopkraan' },
  { value: 'Mobiele torenkraan', viewValue: 'Mobiele torenkraan' },
  { value: 'Rupskraan', viewValue: 'Rupskraan' },
  { value: 'Autolaadkraan', viewValue: 'Autolaadkraan' },
  { value: 'Vrachtauto', viewValue: 'Vrachtauto' },
  { value: 'Bedrijfswagen', viewValue: 'Bedrijfswagen' },
  // { value: 'Graafmachine', viewValue: 'Graafmachine' },
  { value: 'Verreiker', viewValue: 'Verreiker' },
  // {value: 'onbekend', viewValue: 'Onbekend'},
];

export const EmissieNorm = [
  { value: 'onbekend', viewValue: 'Onbekend' },
  { value: 'EURO', viewValue: 'Euro' },
  { value: 'STAGE', viewValue: 'Stage' },
  { value: 'TIER', viewValue: 'Tier' },
];

export const KlasseEuro = [
  { value: 'onbekend', viewValue: 'Onbekend' },
  { value: 'EURO 1', viewValue: 'Euro 1' },
  { value: 'EURO 2', viewValue: 'Euro 2' },
  { value: 'EURO 3', viewValue: 'Euro 3' },
  { value: 'EURO 4', viewValue: 'Euro 4' },
  { value: 'EURO 5', viewValue: 'Euro 5' },
  { value: 'EURO 6', viewValue: 'Euro 6' },
]


export const KlasseStage = [
  { value: 'onbekend', viewValue: 'Onbekend' },
  { value: "STAGE 1", viewValue: 'Stage 1' },
  { value: "STAGE 2", viewValue: 'Stage 2' },
  { value: "STAGE 3A", viewValue: 'Stage 3A' },
  { value: "STAGE 3B", viewValue: 'Stage 3B' },
  { value: "STAGE 4", viewValue: 'Stage 4' },
  { value: 'STAGE 5', viewValue: 'Stage 5' },
]


export const KlasseTier = [
  { value: 'onbekend', viewValue: 'Onbekend' },
  { value: "TIER 1", viewValue: 'Tier 1' },
  { value: "TIER 2", viewValue: 'Tier 2' },
  { value: "TIER 3", viewValue: 'Tier 3' },
  { value: "TIER 4i", viewValue: 'Tier 4i' },
  { value: "TIER 4F", viewValue: 'Tier 4F' },
]

export const typeAandrijving = [
  { value: 'onbekend', viewValue: 'Onbekend' },
  { value: 'constant', viewValue: 'Constant' },
  { value: 'variabel', viewValue: 'Variabel' },
]


export const typeBrandstof = [
  { value: 'Benzine', viewValue: 'Benzine' },
  { value: 'Diesel', viewValue: 'Diesel' },
  { value: 'HVO100', viewValue: 'HVO 100' },
  { value: 'HVOmix', viewValue: 'HVO Mix' },
  { value: 'Biodiesel', viewValue: 'Bio Diesel' },
  { value: 'GTL', viewValue: 'GTL' },
  { value: 'BioEthanol', viewValue: 'Bio Ethanol' },
  { value: 'LPG', viewValue: 'LPG' },
  { value: 'Elektrisch', viewValue: 'Elektrisch' },
  { value: 'Waterstof', viewValue: 'Waterstof' },
]

export const Werkbelasting = [
  { value: 0, viewValue: 'N.v.t.' },
  { value: 20, viewValue: 'Zeer laag' },
  { value: 35, viewValue: 'Laag' },
  { value: 50, viewValue: 'Normaal' },
  { value: 65, viewValue: 'Hoog' },
  { value: 80, viewValue: 'Zeer hoog' },
]

export const TrueFalse = [
  { value: 'true', viewValue: 'Ja' },
  { value: 'false', viewValue: 'Nee' },
]
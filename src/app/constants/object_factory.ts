import { Activity } from "../models/activity"
import { Engine } from "../models/engine"
import { Project } from "../models/project"
import { Vehicle } from "../models/vehicle"
import { VehicleEmission } from "../models/vehicleEmission"

export const empty_Vehicle = (): Vehicle => (
  {
    vehicle_ID: '',
    //vehicleDescription: "",
    brand: "",
    model: "",
    vehicleType: "",
    modelYear: 2000,// jaren!
    weight: 0, // ton
    primaryEngine: empty_Engine(),
    secundairyEngine: empty_Engine(),
    activity: empty_Activity()
  }
)

export const empty_Engine = (): Engine => (
  {
    emissionNorm: 'onbekend',
    emissionClass: 'onbekend',
    driveType: 'onbekend',
    power: 0,
    fuel: '',
    hasSootFilter: false,
    vehicle_id: -1,
  }
)

export const empty_Activity = (): Activity => (
  {
    description: "",
    distance: null, // km
    tol: null, // uren
    dtol: null, // %
    workload: null, // %
  }
)

export const empty_Project = (): Project => (
  {
    id: '',
    companyName: '',
    name: '',
    number: '',
    location: '',
    vehicles: [],
    version: 0,
    createdAt: new Date(),
    downloads: 0
  }
)

export const empty_VehicleEmission = (): VehicleEmission => (
  {
    vehicle: {
      id: "",
      vehicle_ID: "",
      vehicleDescription: "",
      brand: "",
      model: "",
      vehicleType: "",
      modelYear: 0,
      weight: 0,
      primaryEngine: {
        emissionNorm: "",
        emissionClass: "",
        driveType: "",
        power: 0,
        fuel: "",
        hasSootFilter: false,
        vehicle_id: 0
      },
      secundairyEngine: {
        emissionNorm: "",
        emissionClass: "",
        driveType: "",
        power: 0,
        fuel: "",
        hasSootFilter: false,
        vehicle_id: 0
      },
      activity: empty_Activity()
    },
    CO2Emission: {
      driveEmission: 0,
      turnEmission: 0,
      totalEmission: 0
    },
    NOXEmission: {
      driveEmission: 0,
      turnEmission: 0,
      totalEmission: 0
    },
    waste: {
      driveWaste: 0,
      turnWaste: 0,
      L100KM: 0,
      L1U: 0
    }
  }
)
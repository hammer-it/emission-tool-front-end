import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { VehicleDTO } from '../dto/vehicle';
import { Vehicle } from '../models/vehicle';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  // Node/Express API
  GATEWAY: string = environment.API_URL + '/api/vehicles/';
  httpHeaders = new HttpHeaders({
  });
  
  // TIJDELIJK
  private companyId = 1; 

  constructor(private httpClient: HttpClient) {
  }

  getById(companyId = 1, vehicleId): Observable<any> {
    return this.httpClient.get(`${this.GATEWAY}` + companyId+ '/' + vehicleId, { headers: this.httpHeaders });
  }

  getAllVehicles(companyId = 1): Observable<any> {
    return this.httpClient.get(`${this.GATEWAY}` + companyId, { headers: this.httpHeaders });
  }

  getEnginesByVehicleId(companyId = 1 ,vehicleId): Observable<any> {
    return this.httpClient.get(`${this.GATEWAY}` + companyId + '/' + vehicleId + '/engines', { headers: this.httpHeaders })
  }

  createVehicle(vehicle: Vehicle): Observable<any>{
    const dto: VehicleDTO = {
      vehicle_ID: vehicle.vehicle_ID,
      vehicleDescription: vehicle.vehicleDescription,
      brand: vehicle.brand,
      model: vehicle.model,
      vehicleType: vehicle.vehicleType,
      modelYear: vehicle.modelYear,
      weight: vehicle.weight
    }

    let msg = {
      company_id: this.companyId,
      vehicle: dto,
      primaryEngine: vehicle.primaryEngine,
      secundaryEngine: vehicle.secundairyEngine
    }

    return this.httpClient.post( `${this.GATEWAY}`, JSON.stringify(msg), {responseType: 'text', headers: this.httpHeaders});
  }

  deleteVehicle(vehicleId){
    console.log('url: ' + `${this.GATEWAY}` + vehicleId);
    return this.httpClient.delete(`${this.GATEWAY}` + vehicleId, {responseType: 'text', headers: this.httpHeaders });
  }

}
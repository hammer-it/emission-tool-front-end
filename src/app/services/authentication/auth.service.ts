import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { TokenService } from './token.service';

import { catchError, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

const OAUTH_CLIENT = "bca10d30-428d-4144-b5c8-7e6f6516c033" //"28d45478-bdd7-4a58-b5e6-922636ffa604";
const OAUTH_REDIRECTURI = "http://localhost:4200/pages/lock"
// const OAUTH_SECRET = 'express-secret';
const API_URL = 'http://localhost:8080/';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    // 'Content-Type': 'application/x-www-form-urlencoded',
    // Authorization: 'Basic ' + btoa(OAUTH_CLIENT + ':' + OAUTH_SECRET),
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  redirectUrl = '';

  private static handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  private static log(message: string): any {
    console.log(message);
  }

  constructor(private http: HttpClient, private tokenService: TokenService, private route: ActivatedRoute, private router: Router) { }

  login(){
    this.tokenService.removeToken();
    this.tokenService.removeRefreshToken();
    const login_url = API_URL + "oauth2/authorize?response_type=token&client_id="+ OAUTH_CLIENT +"&redirect_uri=" + OAUTH_REDIRECTURI
    window.location.href = login_url
  }

  getAccesToken(){
    const full_url = window.location.href
    const access_token = full_url.substring(full_url.indexOf('=')+1, full_url.indexOf('&'))

    this.tokenService.saveToken( decodeURI(access_token) );
  }

  refreshToken(refreshData: any): Observable<any> {
    this.tokenService.removeToken();
    this.tokenService.removeRefreshToken();
    const body = new HttpParams()
      .set('refresh_token', refreshData.refresh_token)
      .set('grant_type', 'refresh_token');
    return this.http.post<any>(API_URL + 'oauth/token', body, HTTP_OPTIONS)
      .pipe(
        tap(res => {
          this.tokenService.saveToken(res.access_token);
          this.tokenService.saveRefreshToken(res.refresh_token);
        }),
        catchError(AuthService.handleError)
      );
  }

  logout(): void {
    this.tokenService.removeToken();
    this.tokenService.removeRefreshToken();
    this.router.navigate(['/pages/login']).then(_ => false);
  }

  register(data: any): Observable<any> {
    return this.http.post<any>(API_URL + 'oauth/signup', data)
      .pipe(
        tap(_ => AuthService.log('register')),
        catchError(AuthService.handleError)
      );
  }

  secured(): Observable<any> {
    return this.http.get<any>(API_URL + 'secret')
      .pipe(catchError(AuthService.handleError));
  }

}

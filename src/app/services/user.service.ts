import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // Node/Express API
  GATEWAY: string = environment.API_URL + '/api/users/';
  httpHeaders = new HttpHeaders({
    // 'Content-Type': 'application/json',
    // 'Accept': 'application/json',
    // 'Access-Control-Allow-Origin': 'http://localhost:4200/',
    //'Authorization':'Bearer 7ab2fb30375442cb8afd4d8ffa9a82f6|ae8e15841ea744229797c9aabfeba424'
  });

  constructor(private httpClient: HttpClient) {

  }


  getUserByCompany(companyId = "A"): Observable<any> {
    return this.httpClient.get(`${this.GATEWAY}` + companyId, { headers: this.httpHeaders });
  }

  getAllUsers(): Observable<any> {
    return this.httpClient.get(`${this.GATEWAY}`, { headers: this.httpHeaders });
  }

}

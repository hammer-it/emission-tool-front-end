import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { ProjectDTO } from "../dto/project";
import { Project } from "../models/project";

@Injectable({
    providedIn: 'root'
  })
  export class ProjectService {
    // TIJDELIJK
    private companyId = 1; 
    // Node/Express API
    GATEWAY: string = environment.API_URL + '/api/projects/';
    httpHeaders = new HttpHeaders({
    });
    
  
    constructor(private httpClient: HttpClient) {
    }
  
    getById(companyId = 1, projectId): Observable<any> {
      console.log(`${this.GATEWAY}` + companyId+ '/' + projectId);
      return  this.httpClient.get(`${this.GATEWAY}` + companyId+ '/' + projectId, { headers: this.httpHeaders });
    }
  
    getAll(companyId = 1): Observable<any> {
      return this.httpClient.get(`${this.GATEWAY}` + companyId, { headers: this.httpHeaders });
    }
  
    create(project: Project, companyId = 1): Observable<any>{
      let emissionList = [];  
      const dto: ProjectDTO = {
        companyName: project.companyName,
        name: project.name,
        number: project.number,
        location: project.location,
        version: project.version,
        downloads: project.downloads
      }

      project.vehicles.forEach(emission => {
          let em = {
            driveWaste: emission.waste.driveWaste,
            turnWaste: emission.waste.turnWaste,
            L100KM: emission.waste.L100KM,
            L1U: emission.waste.L1U,
            vehicle: {
                vehicle_ID: emission.vehicle.vehicle_ID,
                vehicleDescription:  emission.vehicle.vehicleDescription,
                brand:  emission.vehicle.brand,
                model:  emission.vehicle.model,
                vehicleType:  emission.vehicle.vehicleType,
                modelYear:  emission.vehicle.modelYear,
                weight:  emission.vehicle.weight 
            },
            primaryEngine: {
                emissieNorm: emission.vehicle.primaryEngine.emissionNorm,
                emissionClass:  emission.vehicle.primaryEngine.emissionClass,
                driveType: emission.vehicle.primaryEngine.driveType,
                power:  emission.vehicle.primaryEngine.power,
                fuel:  emission.vehicle.primaryEngine.fuel,
                hasSoothFilter: emission.vehicle.primaryEngine.hasSootFilter,
            },
            secundaryEngine: {
                emissieNorm: emission.vehicle.secundairyEngine.emissionNorm,
                emissionClass:  emission.vehicle.secundairyEngine.emissionClass,
                driveType: emission.vehicle.secundairyEngine.driveType,
                power:  emission.vehicle.secundairyEngine.power,
                fuel:  emission.vehicle.secundairyEngine.fuel,
                hasSoothFilter: emission.vehicle.secundairyEngine.hasSootFilter,
            },
            nox: {
                driveEmission: emission.NOXEmission.driveEmission,
                turnEmission:  emission.NOXEmission.turnEmission,
                totalEmission:  emission.NOXEmission.totalEmission
            },
            co2:{
                driveEmission: emission.CO2Emission.driveEmission,
                turnEmission:  emission.CO2Emission.turnEmission,
                totalEmission:  emission.CO2Emission.totalEmission
            }
          }

          emissionList.push(em);
      })
  
      let msg = {
        companyId: companyId,
        project: dto,
        vehicleEmissions: emissionList
      }
  
      return this.httpClient.post( `${this.GATEWAY}`, JSON.stringify(msg), {responseType: 'text', headers: this.httpHeaders});
    }
  
    delete(projectId){
      console.log('url: ' + `${this.GATEWAY}` + projectId);
      return this.httpClient.delete(`${this.GATEWAY}/` + projectId, {responseType: 'text', headers: this.httpHeaders });
    }
  
  }
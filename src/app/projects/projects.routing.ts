import { Routes } from '@angular/router';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectOverviewComponent } from './project-overview/project-overview.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';



export const ProjectRoutes: Routes = [
  {

    path: '',
    children: [{
      path: 'overview',
      component: ProjectOverviewComponent
    },
    {
      path: 'create',
      component: ProjectCreateComponent
    },
    {
      path: 'details/:id',
      component: ProjectDetailsComponent
    }
  ]
  }
];

import { formatDate } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { empty_Engine, empty_Project, empty_VehicleEmission } from "src/app/constants/object_factory";
import { Engine } from "src/app/models/engine";
import { Project } from "src/app/models/project";
import { VehicleEmission } from "src/app/models/vehicleEmission";
import { ProjectService } from "src/app/services/project.service";

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent implements OnInit {
  loading = false;
  selectedVehicle: VehicleEmission = empty_VehicleEmission();
  selectedVehicleIndex: number = 0;
  //Company: Company = empty_bedrijf();
  project : Project = empty_Project()
  vehicleEmissions = []


  emissiePerBrandstof = {
    koolstofdioxide: 0,
    stikstof: 0,
    Benzine: 0,
    Diesel: 0,
    HVO100: 0,
    HVOmix: 0,
    GTL: 0,
    Biodiesel: 0,
    BioEthanol: 0,
    LPG: 0,
    Elektrisch: 0,
    Waterstof: 0,
  }

  totaleVerbruik = 0;

  public selectedEngine1: Engine = empty_Engine()
  public selectedEngine2: Engine = empty_Engine()
  public date: string = formatDate(new Date(), 'dd/MM/yyyy', 'en');

  brandstof_dashboard_lijst = [];

  constructor(private projectService: ProjectService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id
    this.projectService.getById(1, id).subscribe(data => {
      console.log(data, 'vggg');
      this.project = data.Project
      this.vehicleEmissions = data.VehicleEmissions

      console.log(this.vehicleEmissions[0]);
      this.setEmissiePerBrandstof()
      this.weergaveBrandstofGegevens()
    })

   
  }


  weergaveBrandstofGegevens() {
    var fuels = []
    //Get the three most used fuel types
    //Check all fueltypes
    for (const [key, value] of Object.entries(this.emissiePerBrandstof)) {
      if (key != 'koolstofdioxide' && key != "stikstof") {
        fuels.push({ name: key, value: value })
      }
    }

    fuels = fuels.sort(function (a, b) { return b.value - a.value }).slice(0, 3)

    this.brandstof_dashboard_lijst = fuels;
  }


  setEmissiePerBrandstof(){
    this.vehicleEmissions.forEach((voertuig) => {
      console.log( voertuig.Co2Emission.totalEmission)
      this.emissiePerBrandstof.koolstofdioxide += voertuig.Co2Emission.totalEmission
      this.emissiePerBrandstof.stikstof += voertuig.NoxEmission.totalEmission

      switch (voertuig.primaryEngine.fuel) {
        case 'benzine':
          this.emissiePerBrandstof.Benzine += (voertuig.driveWaste)
          break;
        case 'diesel':
          this.emissiePerBrandstof.Diesel += (voertuig.driveWaste)
          break;
        case 'HVO100':
          this.emissiePerBrandstof.HVO100 += (voertuig.driveWaste)
          break;
        case 'HVOmix':
          this.emissiePerBrandstof.HVOmix += (voertuig.driveWaste)
          break;
        case 'GTL':
          this.emissiePerBrandstof.GTL += (voertuig.driveWaste)
          break;
        case 'Biodiesel':
          this.emissiePerBrandstof.Biodiesel += (voertuig.driveWaste)
          break;
        case 'BioEthanol':
          this.emissiePerBrandstof.BioEthanol += (voertuig.driveWaste)
          break;
        case 'LPG':
          this.emissiePerBrandstof.LPG += (voertuig.driveWaste)
          break;
        case 'Elektrisch':
          this.emissiePerBrandstof.Elektrisch += (voertuig.driveWaste)
          break;
        case 'Waterstof':
          this.emissiePerBrandstof.Waterstof += (voertuig.driveWaste)
          break;
        default:
          break;
      }

      switch (voertuig.secundaryEngine.fuel) {
        case 'benzine':
          this.emissiePerBrandstof.Benzine += (voertuig.turnWaste)
          break;
        case 'diesel':
          this.emissiePerBrandstof.Diesel += (voertuig.turnWaste)
          break;
        case 'HVO100':
          this.emissiePerBrandstof.HVO100 += (voertuig.turnWaste)
          break;
        case 'HVOmix':
          this.emissiePerBrandstof.HVOmix += (voertuig.turnWaste)
          break;
        case 'GTL':
          this.emissiePerBrandstof.GTL += (voertuig.turnWaste)
          break;
        case 'Biodiesel':
          this.emissiePerBrandstof.Biodiesel += (voertuig.turnWaste)
          break;
        case 'BioEthanol':
          this.emissiePerBrandstof.BioEthanol += (voertuig.turnWaste)
          break;
        case 'LPG':
          this.emissiePerBrandstof.LPG += (voertuig.turnWaste)
          break;
        case 'Elektrisch':
          this.emissiePerBrandstof.Elektrisch += (voertuig.turnWaste)
          break;
        case 'Waterstof':
          this.emissiePerBrandstof.Waterstof += (voertuig.turnWaste)
          break;
        default:
          break;
      }

    });

    
    for (const [key, value] of Object.entries(this.emissiePerBrandstof)) {
      // //console.log(key, value)
      if (key != "koolstofdioxide" && key != "stikstof" && key != "fijnstof") {
        this.totaleVerbruik += value;
        //  //console.log(this.totaleVerbruik)
      }
    }
    this.totaleVerbruik = parseFloat(this.totaleVerbruik.toFixed(1))
  }


  openEditProject() { }
  download() { }
  timestampToLocaleDate(date) { 
    return this.date
  }
}
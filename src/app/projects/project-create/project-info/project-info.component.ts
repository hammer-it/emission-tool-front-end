import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { delay, getDisclaimer, showNotification } from "src/app/constants/functions";
import { empty_Project } from "src/app/constants/object_factory";
import { Project } from "src/app/models/project";
import { Vehicle } from "src/app/models/vehicle";
import { DataService } from "src/app/services/data.service";
import { ProjectService } from "src/app/services/project.service";
import swal from 'sweetalert2';

declare var $: any;

@Component({
    selector: 'app-project-info',
    templateUrl: './project-info.component.html',
    styleUrls: ['./project-info.component.css']
})
export class ProjectInfoComponent implements OnInit{ 
    public dataTable = {
        headerRow: [],
        footerRow: [],
        dataRows: []
    };
    
    newProject: Project = empty_Project()
    disclaimer = getDisclaimer();
    vehicles: Vehicle[] = [];
    loading = false;

    ngOnInit(): void {
        var loadGegevens = this.dservice.getData('nieuw-project-gegevens');
        if(loadGegevens != null || loadGegevens != undefined){
            this.newProject = loadGegevens
        }
    
        this.vehicles = this.dservice.getData('nieuw-project-voertuigen');  
        this.setDataTable()  
    }

    constructor(
        private dservice: DataService,
        private projectService: ProjectService,
        public router: Router,  
        ){}
    
    onVehicleAdded(){
        this.setDataTable();
    }

    setDataTable() {
        this.vehicles = this.dservice.getData('nieuw-project-voertuigen');    
    
        $.fn.dataTable.ext.errMode = 'throw';
        $('#selected-vehicles-table').DataTable().destroy();
        this.dataTable = null;
    
        var list = []
        var i = 0;
        this.vehicles.forEach(vehicle => {
          i++
          list.push([i, vehicle.vehicleType, vehicle.brand, vehicle.model, vehicle.weight, vehicle.modelYear, vehicle.vehicle_ID, vehicle.id])
        })
    
        this.dataTable = {
          headerRow: ['', 'Soort voertuig', 'Merk', 'Type', 'Gewicht (ton)', 'Bouwjaar', 'Voertuig-ID'],
          footerRow: ['', 'Soort voertuig', 'Merk', 'Type', 'Gewicht (ton)', 'Bouwjaar', 'Voertuig-ID'],
          dataRows: list
        };
    
        delay(10).then(() => {
          $('#selected-vehicles-table').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
              [10, 25, 50, -1],
              [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Zoeken...",
            },
            columnDefs: [
              { "targets": [7], "searchable": false, "orderable": false, "visible": true }
            ]
    
          });
          const table = $('#selected-vehicles-table').DataTable();
        })
    }

    checkFields(){
        if(this.vehicles == null || this.vehicles == undefined){
          showNotification('center','Er zijn geen voertuigen toegevoegd aan het project.', 'danger');
          return
        }
        for (let index = 0; index < this.vehicles.length; index++) {
          this.newProject.vehicles.push(
            {
              vehicle: this.vehicles[index],
              CO2Emission: {
                driveEmission: 0,
                turnEmission: 0,
                totalEmission: 0
              },
              NOXEmission: {
                driveEmission: 0,
                turnEmission: 0,
                totalEmission: 0
              },
              waste: {
                driveWaste: 0,
                turnWaste: 0,
                L100KM: 0,
                L1U: 0
              }
            })
        
        }
    
        var canContinue = false
        if(this.newProject.vehicles.length < 1 ){
          canContinue = false;
          showNotification('center','Voeg eerst voertuigen toe aan dit project', 'danger');
        }
        else if(this.newProject.name.length < 1 || this.newProject.number.length < 1 || this.newProject.location.length < 1){
          canContinue = false;
          showNotification('center','Vul eerst alle project gegevens in.', 'danger');
        }else{
            canContinue = true;
            this.newProject.companyName =  "testbv" //TIJDELIJK
            this.newProject.createdAt = new Date()
            this.newProject.version = 1
      
            this.createProject();
            showNotification('center','Project aangemaakt', 'success');
        }
    }

    createProject(){
        this.loading = true
        // /  this.newProject.vehicles = this.vehicles
        this.newProject.downloads = 0;
        const sub = this.projectService.create(this.newProject).subscribe(res => {
            this.dservice.setData('nieuw-project-voertuigen', null);
            this.dservice.setData('nieuw-project-gegevens', null);
            this.newProject = empty_Project();
            this.loading = false
            sub.unsubscribe();
            //this.router.navigate(["/app/project-details/" + res.id + '/0']);
        })
    }

}
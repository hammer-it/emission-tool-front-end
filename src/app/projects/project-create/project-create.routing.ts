import { Routes } from '@angular/router';

import { ProjectInfoComponent } from './project-info/project-info.component'
import { VehiclesComponent} from './vehicles/vehicles.component';

export const ProjecCreateRoutes: Routes = [
    {path: 'project-info', component: ProjectInfoComponent},   
    {path: 'project-vehicles', component: VehiclesComponent},
];

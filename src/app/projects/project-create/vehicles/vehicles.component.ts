import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { deepCopy, delay, showNotification } from "src/app/constants/functions";
import { typeVoertuig, EmissieNorm, KlasseEuro, KlasseStage, KlasseTier, typeAandrijving, typeBrandstof, Werkbelasting } from "src/app/constants/KeuzeLijsten";
import { empty_Activity, empty_Engine, empty_Vehicle } from "src/app/constants/object_factory";
import { GEWICHTTIP, AANDRIJVINGTIP, AFSTANDTIP, FILTERTIP, VERMOGENTIP, MOTORWERKZAAMHEDENTIP } from "src/app/constants/tooltips";
import { Activity } from "src/app/models/activity";
import { Engine } from "src/app/models/engine";
import { Vehicle } from "src/app/models/vehicle";
import { DataService } from "src/app/services/data.service";
import { VehicleService } from "src/app/services/vehicle.service";

declare var $: any;

@Component({
    selector: 'app-vehicles',
    templateUrl: './vehicles.component.html',
    styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit{
    public dataTable = {
        headerRow: [],
        footerRow: [],
        dataRows: []
    };
    public vehicles: Vehicle[] = [];
    public allVehicles: Vehicle[] = [];
    public selectedEngine1: Engine = empty_Engine()
    public selectedEngine2: Engine = empty_Engine();
    public heeftSecundaireMotor = false;
    public vehicle: Vehicle = empty_Vehicle()
    public new_activity: Activity = empty_Activity()
    public EK = []
    public EK2 = []
    
    @Output() vehicleAdded = new EventEmitter<any>();

    // ENUMS
    typeVoertuig = typeVoertuig
    EmissieNorm = EmissieNorm
    EmissieKlasseInit = KlasseEuro.concat(KlasseStage).concat(KlasseTier)
    typeAandrijving = typeAandrijving
    typeBrandstof = typeBrandstof
    werkbelasting = Werkbelasting

    // TIPS
    GEWICHTTIP = GEWICHTTIP;
    AANDRIJVINGTIP = AANDRIJVINGTIP
    AFSTANDTIP = AFSTANDTIP
    FILTERTIP = FILTERTIP
    VERMOGENTIP=VERMOGENTIP
    MOTORWERKZAAMHEDENTIP=MOTORWERKZAAMHEDENTIP 


    ngOnInit(): void {
        this.getAllVehicles();
        this.EK = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm)
        this.EK2 = this.initEmissionClasses(this.vehicle.secundairyEngine.emissionNorm)
    }

    constructor(
        private service: VehicleService,
        private dservice: DataService
    ){}

    getAllVehicles() {
        this.service.getAllVehicles().subscribe(data => {
            var i = 0;
            var list = [];
            this.allVehicles = data;
            this.vehicles = this.allVehicles
            this.vehicles.sort(
              function (c, d) {
                if (c.vehicleType > d.vehicleType) { return -1; }
                if (c.vehicleType < d.vehicleType) { return 1; }
                return 0;
              });
              this.vehicles.forEach(vehicle => {
                i++
                list.push([i, vehicle.vehicleType, vehicle.brand, vehicle.model, vehicle.weight, vehicle.modelYear, vehicle.vehicle_ID, vehicle.id])
              })
              this.setDataTable(list);
          })
    }

    initEmissionClasses(norm) {
        // //console.log("norm" + norm)
        var list = [];
        switch (norm) {
          case "EURO":
            list = KlasseEuro
            return list;
          case "STAGE":
            list = KlasseStage
            return list;
          case "TIER":
            list = KlasseTier
            return list;
          case "onbekend":
            list = [{ value: 'onbekend', viewValue: 'Onbekend' },]
            return list;
        //  default:
        // /    list = KlasseEuro
          //  return list;
        }
    }

    setDataTable(list) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#wagenpark-table').DataTable().destroy();
        this.dataTable = null;
    
        this.dataTable = {
          headerRow: [ 'nr.', 'Soort voertuig', 'Merk', 'Type', 'Gewicht (ton)', 'Bouwjaar', 'Voertuig-ID' ],
          footerRow: [ 'nr.', 'Soort voertuig', 'Merk', 'Type', 'Gewicht (ton)', 'Bouwjaar', 'Voertuig-ID' ],
          dataRows: list
        };  
    
        delay(100).then(()=> {
          $('#wagenpark-table').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
              [10, 25, 50, -1],
              [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
              search: "_INPUT_",
              searchPlaceholder: "Zoeken...",
            },
            columnDefs: [
            //{"targets": [7], "searchable": false, "orderable": false, "visible": true }
            ]
        
          });
          const table = $('#wagenpark-table').DataTable();
        })
    }

    switchEmissionClass(engine) {
        //console.log(engine)
        if (engine == 1) {
          this.EK = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm)
        } else {
          this.EK2 = this.initEmissionClasses(this.vehicle.secundairyEngine.emissionNorm)
        }
    }

    toggleSecundaireMotor() {
        if (this.heeftSecundaireMotor) {
          this.heeftSecundaireMotor = false
        } else {
          this.heeftSecundaireMotor = true
        }
    }

    cancelSelect(){
        this.vehicle = empty_Vehicle();
        this.EK = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm)
        this.EK2 = this.initEmissionClasses(this.vehicle.secundairyEngine.emissionNorm)
    }
    
    closeModal2(){
    document.getElementById('chooseVehicleModal2').click();
    }

    checkFieldsAndAddVehicle() {
        console.log('1')
        var canContinue = false;
        this.vehicle.activity = this.new_activity;

        if(this.vehicle.activity.dtol > 100){
            this.vehicle.activity.dtol = 100
        }

        if (this.vehicle.brand == "" || this.vehicle.model == "" || this.vehicle.modelYear == 0 || this.vehicle.vehicleType == "" || this.vehicle.weight < 0 || this.vehicle.activity.description.length < 1 || this.vehicle.activity.distance < 0 || this.vehicle.activity.dtol < 0 || this.vehicle.activity.tol < 0 || this.vehicle.activity.workload < 0) {
            showNotification('center',"vul eerst alle verplichte velden in.", 'danger')
            canContinue = false;
            return canContinue;
        }

        console.log(this.vehicle.primaryEngine)
        for (const [key, value] of Object.entries(this.vehicle.primaryEngine)) {
            //  //console.log(value)
            if (value === "" || value === 0) {
            //console.log(value)
            showNotification('center',"Primaire motor niet correct ingevuld.", 'danger')
            canContinue = false
            return canContinue;
            }
        }

            //Check if the information about the secundary engine is filled in
        if(!this.heeftSecundaireMotor){
            this.vehicle.secundairyEngine = deepCopy(this.vehicle.primaryEngine)
        }   

        for (const [key, value] of Object.entries(this.vehicle.secundairyEngine)) {
        if (value === "" || value === 0 || value == null) {
            //console.log(value)
            showNotification("right","Motor werkzaamheden niet correct ingevuld.", 'warning')
            canContinue = false
            return canContinue;
        }
        }

        //als hij draai werkzaamheden uitvoert:
        if(this.vehicle.activity.dtol || this.vehicle.activity.workload){
            if(!this.vehicle.activity.dtol || !this.vehicle.activity.workload || !this.vehicle.activity.tol || this.vehicle.activity.workload <= 0){
            showNotification('center',"Er is geen draaitijd, werkbelasting of tijd op locatie ingevuld. Werkbelasting kan niet nul zijn bij werkzaamheden.", 'danger')
            canContinue = false
            return canContinue;
            }
        }

        //Geen werkzaamheden en geen rijafstand
        if(this.vehicle.activity.workload <= 0 && this.vehicle.activity.distance <= 0){
            showNotification('center',"Er zijn geen werkzaamheden en geen rijafstand ingevuld.", 'danger')
            canContinue = false
            return canContinue;
        }

        console.log('2')
        canContinue = true

        if (canContinue) {
            console.log('3')
            var vehicles = []
            vehicles = this.dservice.getData('nieuw-project-voertuigen');
            if (vehicles == undefined || vehicles == null) {
                vehicles = []
            }
            var vehicle = Object.assign({}, this.vehicle)
            if(vehicle.activity.dtol > 100){
                vehicle.activity.dtol = 100
            }
            vehicles.push(vehicle);
            
            console.log('4')
            this.dservice.setData('nieuw-project-voertuigen', vehicles);

            showNotification('center','Voertuig toegevoegd aan nieuw project', 'success')
            this.vehicle = empty_Vehicle()
            this.vehicle.activity = empty_Activity()
            this.new_activity = empty_Activity()

            //sluit de modal
            document.getElementById('chooseVehicleModal1').click();
            this.vehicleAdded.emit();
            
            return true
        } else {
            return false
        }
    }
    
}
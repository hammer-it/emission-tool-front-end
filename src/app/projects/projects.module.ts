import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { ProjectRoutes } from './projects.routing';
import { ProjectOverviewComponent } from './project-overview/project-overview.component';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectCreateModule } from './project-create/project-create.module';




@NgModule({
  declarations: [ProjectOverviewComponent, ProjectCreateComponent, ProjectDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ProjectRoutes),
    FormsModule,
    MaterialModule,
    ProjectCreateModule
  ]
})
export class ProjectModule { }

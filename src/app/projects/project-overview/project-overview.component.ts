import { Component, OnInit } from '@angular/core';
import { delay } from 'src/app/constants/functions';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';

declare var $: any;

@Component({
  selector: 'app-project-overview',
  templateUrl: './project-overview.component.html',
  styleUrls: ['./project-overview.component.css']
})
export class ProjectOverviewComponent implements OnInit {
  public dataTable = {
    headerRow: [],
    footerRow: [],
    dataRows: []
  };
  public projects: Project[]

  loading = true;

  constructor(private service: ProjectService) { }

  ngOnInit(): void {
    this.loading = true;
    try{
      console.log('*')
      this.service.getAll().subscribe(res => {
        console.log(res)
        if (!res) {
          this.loading = false
          return
        }

        console.log(res)
  
        var list = [];
        var i = 0;
  
        this.projects = res
        this.projects.sort(
        function (c, d) {
          if ((c.createdAt as any).seconds > (d.createdAt as any).seconds) { return -1; }
          if ((c.createdAt as any).seconds < (d.createdAt as any).seconds) { return 1; }
          return 0;
        });
        this.projects.forEach(project => {
          i++
          var viewDate = new Date(project.createdAt).toDateString();
          list.push([i, project.name, project.number, project.location, project.version, viewDate, project.id])
        })
        //2x fixt de bug
        //this.setDataTable(list)
        this.setDataTable(list)
      })
    }catch(err){
      console.log(err)
      this.loading = false;
      throw err;
    }
  }

  setDataTable(list) {
    $.fn.dataTable.ext.errMode = 'throw';
    $('#project-table').DataTable().destroy();
    this.dataTable = null;


    this.dataTable = {
      headerRow: ['', 'Naam', 'Kenmerk', 'Locatie', 'Versie', 'Aanmaakdatum'],
      footerRow: ['', 'Naam', 'Kenmerk', 'Locatie', 'Versie', 'Aanmaakdatum'],
      dataRows: list
    };

    delay(100).then(() => {
      $('#project-table').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [25, 50, -1],
          [25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Zoeken...",
        },
        columnDefs: [
          { "targets": [6], "searchable": false, "orderable": false, "visible": true },
        ]
      });

      const table = $('#project-table').DataTable();
      this.loading = false
    })
  }
}

import { Component, ElementRef, OnInit } from '@angular/core';
import { showNotification } from 'src/app/constants/functions';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  test: Date = new Date();
  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;

  emailAdress = ""

  hideSubscribeBtn = false;

  constructor(private element: ElementRef) {
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
  }

  ngOnInit(): void {
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    body.classList.add('off-canvas-sidebar');
    const card = document.getElementsByClassName('card')[0];
  }

  sendEmail() {
    console.log(this.emailAdress);
    const xmlHttp = new XMLHttpRequest();
    const theUrl = 'https://europe-west1-emissietool-semester-6.cloudfunctions.net/SendEmail?email=' + this.emailAdress;
    xmlHttp.onreadystatechange = function () {
      if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
        console.log(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
    this.hideSubscribeBtn = true

    showNotification('center', 'U bent aangemeld.', 'success')
  }

}

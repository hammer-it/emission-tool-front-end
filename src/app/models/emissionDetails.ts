export class EmissionDetails {
    driveEmission: number // kg
    turnEmission: number // kg
    totalEmission: number // kg
}
export interface Activity {
    description: string;
    distance: number; // km
    tol: number; // uren
    dtol: number; // %
    workload: number; // %
}
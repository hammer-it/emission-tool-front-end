import { Activity } from "./activity";
import { Engine } from "./engine";

export class Vehicle{
    id?: string;
    vehicle_ID: string; //Ingevulde kenteken / uniek nummer
    vehicleDescription?: string;
    brand: string;
    model: string;
    vehicleType: string;
    modelYear: number; // jaren!
    weight: number // ton
    primaryEngine: Engine;
    secundairyEngine: Engine;
    activity: Activity;
}
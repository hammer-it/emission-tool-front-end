import { EmissionDetails } from "./emissionDetails";
import { Vehicle } from "./vehicle";
import { WasteDetails } from "./wasteDetails";

export class VehicleEmission {
    vehicle: Vehicle;
    CO2Emission: EmissionDetails;
    NOXEmission: EmissionDetails;
    waste: WasteDetails;
}
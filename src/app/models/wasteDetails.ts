
export class WasteDetails {
    driveWaste: number // Liters verbruik bij rijden
    turnWaste: number // " " draaien
    L100KM: number; // Liter per 100 km
    L1U: number // Liter per uur *Alleen voor het draaien van toepassing
}
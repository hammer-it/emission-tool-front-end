export class Engine{
    emissionNorm: string;
    emissionClass: string;
    driveType: string;
    power: number; //KWH kilo Watt uur
    fuel: string;
    hasSootFilter: boolean;
    vehicle_id:number
}
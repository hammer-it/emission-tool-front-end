import { VehicleEmission } from "./vehicleEmission";


export class Project {
    id: string;
    companyName: string;
    name: string;
    number: string;
    location: string;
    vehicles: VehicleEmission[];
    version: number;
    versionTitle?: string;
    createdAt: Date;
    updatedAt?: Date;
    downloads: number;
}
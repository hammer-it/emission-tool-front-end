import { Component, OnInit } from '@angular/core';
import { deepCopy } from 'src/app/constants/functions';
import { EmissieNorm, KlasseEuro, KlasseStage, KlasseTier, typeAandrijving, typeBrandstof, typeVoertuig } from 'src/app/constants/KeuzeLijsten';
import { empty_Vehicle } from 'src/app/constants/object_factory';
import { GEWICHTTIP, AANDRIJVINGTIP, AFSTANDTIP, FILTERTIP, VERMOGENTIP, MOTORWERKZAAMHEDENTIP } from 'src/app/constants/tooltips';
import { VehicleService } from 'src/app/services/vehicle.service';

declare var $: any;

@Component({
  selector: 'app-vehicle-create',
  templateUrl: './vehicle-create.component.html',
  styleUrls: ['./vehicle-create.component.css']
})
export class VehicleCreateComponent implements OnInit {
  public EK = [];
  public EK2 = []; 
  public heeftSecundaireMotor = false;
  public vehicle = empty_Vehicle();

  simpleSlider = 40;
  doubleSlider = [20, 60];
  tooltips = [];
  touch: boolean;

  selectedValue: string;
  currentCity: string[];

  selectTheme = 'primary';

  // Enumerators
  TypeVoertuig = typeVoertuig
  EmissieNorm = EmissieNorm
  typeAandrijving = typeAandrijving
  typeBrandstof = typeBrandstof

  // Tips
  GEWICHTTIP = GEWICHTTIP;
  AANDRIJVINGTIP = AANDRIJVINGTIP
  AFSTANDTIP = AFSTANDTIP
  FILTERTIP = FILTERTIP
  VERMOGENTIP=VERMOGENTIP
  MOTORWERKZAAMHEDENTIP=MOTORWERKZAAMHEDENTIP 
  
  constructor(private service: VehicleService) { }

  ngOnInit(): void {
    this.EK = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm)
    this.EK2 = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm);
  }

  initEmissionClasses(norm) {
    // //console.log("norm" + norm)
    var list = [{ value: 'onbekend', viewValue: 'Onbekend' },];
    switch (norm) {
      case "EURO":
        list = KlasseEuro
        return list;
      case "STAGE":
        list = KlasseStage
        return list;
      case "TIER":
        list = KlasseTier
        return list;
      case "onbekend":
        list = [{ value: 'onbekend', viewValue: 'Onbekend' },]
        return list;
      default:
        list = KlasseEuro
        return list;
    }
  }

  switchEmissionClass(engine) {
    //console.log(engine)
    if (engine == 1) {
      this.EK = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm)
    } else {
      this.EK2 = this.initEmissionClasses(this.vehicle.secundairyEngine.emissionNorm)
    }
  }

  addVehicle(){
    this.service.createVehicle(this.vehicle).subscribe(() => {
      this.presentToast("Machine toegevoegd aan uw wagenpark", 'success')
    })
  }

  checkFields() {
    var canContinue = false;
    //Check if the basic information about the verhicle is filled in
    if (this.vehicle.brand == "" || this.vehicle.model == "" || this.vehicle.modelYear == 0 || this.vehicle.vehicleType == "" || this.vehicle.weight < 0 || this.vehicle.vehicle_ID == "") {
      this.presentToast("vul eerst alle verplichte velden * in.", 'warning')
      canContinue = false;
      return canContinue;
    }

    //Check if the information about the primary engine is filled in
    for (const [key, value] of Object.entries(this.vehicle.primaryEngine)) {
      if (value === "" || value === 0 || value == null) {
        //console.log(value)
        this.presentToast("Motor rijden niet correct ingevuld.", 'warning')
        canContinue = false
        return canContinue;
      }
    }

    if(!this.heeftSecundaireMotor){
        this.vehicle.secundairyEngine = deepCopy(this.vehicle.primaryEngine)
    }

  //Check if the information about the secundary engine is filled in
    for (const [key, value] of Object.entries(this.vehicle.secundairyEngine)) {
      if (value === "" || value === 0 || value == null) {
        //console.log(value)
        this.presentToast("Motor werkzaamheden niet correct ingevuld.", 'warning')
        canContinue = false
        return canContinue;
      }
    }
    canContinue = true

    if (canContinue) {
      this.presentToast('Het voertuig is toegevoegd aan het wagenpark.', 'success')
      return true
    }
  }

  toggleSecundaireMotor() {
    if (this.heeftSecundaireMotor) {
      this.heeftSecundaireMotor = false
    } else {
      this.heeftSecundaireMotor = true
    }
  }

  presentToast(msg, color) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const randomColor = Math.floor((Math.random() * 4) + 1);
    $.notify({
      icon: "notifications",
      message: msg
    }, {
      type: color,
      timer: 1000,
      placement: {
        from: 'top',
        align: 'center'
      },
      template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
        '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
        '<i class="material-icons" data-notify="icon">notifications</i> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }

}

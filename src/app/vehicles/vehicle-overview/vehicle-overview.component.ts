import { Component, OnInit } from '@angular/core';
import { typeVoertuig, EmissieNorm, typeAandrijving, typeBrandstof, KlasseEuro, KlasseStage, KlasseTier } from 'src/app/constants/KeuzeLijsten';
import { empty_Engine, empty_Vehicle } from 'src/app/constants/object_factory';
import { GEWICHTTIP, AANDRIJVINGTIP, AFSTANDTIP, FILTERTIP, VERMOGENTIP, MOTORWERKZAAMHEDENTIP } from 'src/app/constants/tooltips';
import { Vehicle } from 'src/app/models/vehicle';
import { VehicleService} from '../../services/vehicle.service';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;

@Component({
  selector: 'app-vehicle-overview',
  templateUrl: './vehicle-overview.component.html',
  styleUrls: ['./vehicle-overview.component.css']
})
export class VehicleOverviewComponent implements OnInit {
  public dataTable: DataTable;
  public selectedEngine1 = empty_Engine();
  public selectedEngine2 = empty_Engine();
  public vehicle: Vehicle = empty_Vehicle();
  vehicles = [];
  EK;
  EK2;
  

  // Enumerators
  TypeVoertuig = typeVoertuig
  EmissieNorm = EmissieNorm
  typeAandrijving = typeAandrijving
  typeBrandstof = typeBrandstof

  // Tips
  GEWICHTTIP = GEWICHTTIP;
  AANDRIJVINGTIP = AANDRIJVINGTIP
  AFSTANDTIP = AFSTANDTIP
  FILTERTIP = FILTERTIP
  VERMOGENTIP=VERMOGENTIP
  MOTORWERKZAAMHEDENTIP=MOTORWERKZAAMHEDENTIP 

  constructor(private vehicleService: VehicleService) { }

  ngOnInit() {
    this.vehicleService.getAllVehicles().subscribe(res => {
      var datarow_list = []
      console.log('*');
      this.vehicles = res;
      console.log(res)
      for (let index = 0; index < res.length; index++) {
        var type = res[index].vehicleType
        var brand = res[index].brand
        var model = res[index].model
        var weight = res[index].weight
        var modelYear = res[index].modelYear
        var vehicleId = res[index].vehicleId
        datarow_list.push([index, type,brand,model,weight,modelYear,vehicleId, res[index].id])
      }
      this.dataTable = {
        headerRow: ['','Soort voertuig', 'Merk', 'Type', 'Gewicht (ton)', 'Bouwjaar', 'Voertuig-ID'],
        footerRow: ['','Soort voertuig', 'Merk', 'Type', 'Gewicht (ton)', 'Bouwjaar', 'Voertuig-ID'],
        dataRows: datarow_list
      };
    })
  }

  ngAfterViewInit() {
    $('#vehicle-datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#vehicle-datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      let $tr = $(this).closest('tr');
      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + data[3] + data[4] + data[5] +'\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on edit button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

  deleteVehicle(id){
    console.log(id)
    this.vehicleService.deleteVehicle(id).subscribe(res => {
      console.log('sub');
      console.log(res);
    })
  }

  selectVehicle(id){
    var alle = document.getElementsByClassName("tr-wagenpark");
    for (let index = 0; index < alle.length; index++) {
      var elementid = alle[index].id
      document.getElementById(elementid).style.backgroundColor = "#fff";
    }

    document.getElementById("wagenpark" + id).style.backgroundColor = "rgba(0,0,0,.05)";

    let sub = this.vehicleService.getEnginesByVehicleId(1, id).subscribe(res => {
      console.log(res)
      this.selectedEngine1 = res[0];
      this.selectedEngine2 = res[1];
      this.EK = this.initEmissionClasses(this.vehicle.primaryEngine.emissionNorm)
      this.EK2 = this.initEmissionClasses(this.vehicle.secundairyEngine.emissionNorm)

      sub.unsubscribe()
    });
  }

  initEmissionClasses(norm) {
    // //console.log("norm" + norm)
    var list = [];
    switch (norm) {
      case "EURO":
        list = KlasseEuro
        return list;
      case "STAGE":
        list = KlasseStage
        return list;
      case "TIER":
        list = KlasseTier
        return list;
      case "onbekend":
        list = [{ value: 'onbekend', viewValue: 'Onbekend' },]
        return list;
      default:
        list = KlasseEuro
        return list;
    }
  }
}

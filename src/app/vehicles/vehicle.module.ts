import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VehicleRoutes } from './vehicle.routing';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { VehicleCreateComponent } from './vehicle-create/vehicle-create.component';
import { VehicleOverviewComponent } from './vehicle-overview/vehicle-overview.component';



@NgModule({
  declarations: [VehicleCreateComponent, VehicleOverviewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(VehicleRoutes),
    FormsModule,
    MaterialModule
  ]
})
export class VehicleModule { }

import { Routes } from '@angular/router';

import { VehicleCreateComponent } from './vehicle-create/vehicle-create.component';
import { VehicleOverviewComponent } from './vehicle-overview/vehicle-overview.component';


export const VehicleRoutes: Routes = [

  {

    path: '',
    children: [{
      path: 'overview',
      component: VehicleOverviewComponent
    },
    {
      path: 'create',
      component: VehicleCreateComponent
    }
  ]
  }
];

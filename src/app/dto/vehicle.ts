export class VehicleDTO{
    vehicle_ID: string;
    vehicleDescription?: string;
    brand: string;
    model: string;
    vehicleType: string;
    modelYear: number; 
    weight: number 
}
export class ProjectDTO {
    companyName: string;
    name: string;
    number: string;
    location: string;
    version: number;
    downloads: number;
}
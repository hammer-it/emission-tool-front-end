import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

declare interface DataTable {
  headerRow: string[];
  footerRow: string[];
  dataRows: string[][];
}

declare const $: any;


@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  public dataTable: DataTable;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserByCompany().subscribe(res => {
      var datarow_list = []
      for (let index = 0; index < res.length; index++) {
        console.log(res[index]);
        
        var naam = res[index].firstName + ' ' + res[index].lastName
        var email = res[index].email
        var role = res[index].role
        datarow_list.push([naam, email, role])
      }
      this.dataTable = {
        headerRow: ['Naam', 'Email', 'Rol', 'Actie'],
        footerRow: ['Naam', 'Email', 'Rol', 'Actie'],
        dataRows: datarow_list
      };
    })
  }

  ngAfterViewInit() {
    $('#accounts-datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }

    });

    const table = $('#accounts-datatables').DataTable();

    // Edit record
    table.on('click', '.edit', function (e) {
      let $tr = $(this).closest('tr');
      if ($($tr).hasClass('child')) {
        $tr = $tr.prev('.parent');
      }

      var data = table.row($tr).data();
      alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      e.preventDefault();
    });

    // Delete a record
    table.on('click', '.remove', function (e) {
      const $tr = $(this).closest('tr');
      table.row($tr).remove().draw();
      e.preventDefault();
    });

    //Like record
    table.on('click', '.like', function (e) {
      alert('You clicked on edit button');
      e.preventDefault();
    });

    $('.card .material-datatables label').addClass('form-group');
  }

}

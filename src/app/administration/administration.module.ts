import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationRoutes } from './administration.routing';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { AccountsComponent } from './accounts/accounts.component';
import { InboxComponent } from './inbox/inbox.component';



@NgModule({
  declarations: [AccountsComponent, InboxComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(AdministrationRoutes),
    FormsModule,
    MaterialModule
  ]
})
export class AdministrationModule { }

import { Routes } from '@angular/router';
import { AccountsComponent } from './accounts/accounts.component';
import { InboxComponent } from './inbox/inbox.component';


export const AdministrationRoutes: Routes = [
  {

    path: '',
    children: [{
      path: 'accounts',
      component: AccountsComponent
    },
    {
      path: 'inbox',
      component: InboxComponent
    }
  ]
  }
];
